## Infrastructure deployment and configuration

This is an example of how to deploy an infrastructure on vmware using gitlab, terraform and ansible and how to preserve the state of the infrastructure when you work as a team by using gitlab as a backend for terraform-state file.

## tools

- **terraform** is used to deploy the infrastructure and it is well integrated to gitlab which make deployment easier.
- **Ansible** is used to configure the whole infrastructure, the example here is a simple vm in which we resize disk, change password, generate ssh keys and update
- **gitlab-ci** to make a pipeline which automate the process so we focus only on our code and everything is done automatically

### Gitlab Variables

Some sensitive informations like passwords, can't be written on the code so we use variables instead that are replaced with the right values when our pipeline is executed.
For this example we create the variables below in settings-->CI/CD-->Variables 
- SSH_PRIVATE_KEY
- vsphere_user
- vsphere_password
- vsphere_datacenter
- vsphere_cluster
- vsphere_datastore
- vsphere_folder
- vsphere_template

## Terraform for vmware

For terraform I use separate files for providers and other resources, I also use separate files for variables and main program

```bash
terraform
├── backend.tf
├── Linux.auto.tfvars
├── Linux.main.tf
├── Linux.vars.tf
├── Provider.auto.tfvars
├── Provider.folder.tf
├── Provider.main.tf
└── Provider.vars.tf

0 directories, 8 files
```
For usage with vmware the provider muste be set as `vsphere` and have vsphere server, login and password entries.

```terraform
provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}
```
### Sharing terraform state

When we have a team working on the same vmware (or other) infrastructure it's important that every member has always its last state. Terraform saves its state on a file called `terraform.tfstate` and this file is written everytime a change occurs to the infrastructure. For team work terraform provides backends to store state files and by the way to share it with the team members you can find the types of backend provided by terrofrm at https://www.terraform.io/language/settings/backends
We will use http backend as gitlab provide an http backend for terraform.

```bash
cat backend.tf
```

```terraform
terraform {
    backend "http" {}
}
```
**Self signed certificate**

For self signed certificates add `skip_cert_verification = true`

```terraform
terraform {
    backend "http" {
        skip_cert_verification = true
    }
}
```
## Ansible


Ansible is used to configure our machine once it's provisioned what we will do is resize disk because our template have only 5G update template password with a new password and upgrade the system

```bash
├── Ansible
│   ├── dist-upgrade.yml
│   ├── inventory.yml
│   ├── ResizeLinuxDisks.yml
│   ├── resize.sh
│   ├── sshkeygen.yml
│   └── updatePassword.yml
```
### Variables 

Some parameters will be passed as arguments like

- **remote_host** : host, group or list of hosts on which the playbook will be applied
- **remote_login** : ansible remote user
- **password** : New password to update default template password

To be able to play playbooks, we have to add new variable in settings-->CI/CD-->Variables with type file and create our inventory there it can be ini format or yaml format (see ansible documentation for more details about how to build your inventory https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

## CI/CD pipeline

We want to automate all these stuff and have our infrastructure ready to go after pushing our code in our repository. In gitlab to create and trigger CI/CD pipelines a `.gitlab-ci` file must be created in the root directory of our project.

### Variables

- TF_ROOT: Terrafom Root Directory here we use predefined variable CI_PROJECT_DIR to indicate our project's root directory
- TF_ADDRESS: Backend address in which our tfstate file will be saved again we use some predefined variable to generate our path
- ANSIBLE_ROOT: Ansible playbooks' directory

### before_script

The before script part is used to go to terraform directory and then replace entries with their valeus stored in gitlab as we don't want to show these data here the before_script is global for all stages.

```yaml
before_script:
  - cd ${TF_ROOT}
  - sed -i "s/__vspherepassword__/$vsphere_password/g" ./Provider.vars.tf
  - sed -i "s/__vm_host_password__/$vm_host_password/g" ./Linux.auto.tfvars
  - sed -i "s/__vm_host_password2__/$vm_host_password2/g" ./Linux.auto.tfvars
  - sed -i "s/__vsphereURL__/$vsphere_server/g" ./Provider.auto.tfvars
  - sed -i "s/__vsphereUser__/$vsphere_user/g" ./Provider.auto.tfvars
  - sed -i "s/__vspehereDatacenter__/$vsphere_datacenter/g" ./Provider.auto.tfvars
  - sed -i "s/__vsphereCluster__/$vsphere_cluster/g" ./Provider.auto.tfvars
  - sed -i "s/__vsphereDataStore__/$vsphere_datastore/g" ./Provider.auto.tfvars
  - sed -i "s/__vsphereTemplate__/$vsphere_template/g" ./Provider.auto.tfvars
  - sed -i "s/__vmFolder__/$vsphere_folder/g" ./Provider.folder.tf
```


### Stages

Stages are different steps of our pipeline execution

- prepare : for terraform init, terraform will download the right provider plugin
- validate : check if our syntax is valide
- build : Generate the plan that will be applied and display the changes that will occure
- deploy : Apply the plan and create the desired infrastructure (manually triggered )
- configure : Configure our VM by executing the playbooks we wrote
- destroy : destroy our infrstrcution (manually triggered)

```yaml
stages:
  - prepare
  - validate
  - build
  - deploy
  - configure
  - destroy
```

### Configure Job

The configure wont be executed unless apply job is triggered and success because if the deployment fails it won't be any machine to be configured for that we use the `needs` attribute to create the dependency.

```yaml
configure:
  stage: configure
  image: tba77/ansible:2.12.1
  needs:
    - job: apply
  environment:
    name: production
```

Configure job has two phases :

- **before_script** : in which we check if openssh-client is installed and working, then we add the server fingerprint in our ssh_known_hosts to be able to connect without prompting the finger print that we must accept manually and connect to the server with our private key stored as a variable.

```yaml
  before_script:
    - "command -v ssh-agent >/dev/null || ( apt update && apt install openssh-client -y )"
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $ServerIP >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - echo ${CI_PROJECT_DIR}
    - cd ${ANSIBLE_ROOT}
```

- **script** : In which we execute our ansible playbooks to configure our server(s) notice that we used `CI_PROJECT_NAME` as `remote_host` for this example as we have only one machine for this gitlab project so it makes sense :)

```yaml
script:
    - ansible-playbook -i $INVENTORY ResizeLinuxDisks.yml --extra-vars "remote_host=${CI_PROJECT_NAME}-Srv1 remote_login=root swap_size=${swap}"
    - ansible-playbook -i $INVENTORY updatePassword.yml --extra-vars "remote_host=${CI_PROJECT_NAME}-Srv1 remote_login=root newpassword=${vm_host_password2} user_name=root"
    - ansible-playbook -i $INVENTORY sshkeygen.yml --extra-vars "remote_host=${CI_PROJECT_NAME}-Srv1 remote_login=root"
    - ansible-playbook -i $INVENTORY dist-upgrade.yml --extra-vars "remote_host=${CI_PROJECT_NAME}-Srv1 remote_login=root"
```

Hope this will be useful for the ones who haven't access to the cloud and have to work with vmware on premise.
